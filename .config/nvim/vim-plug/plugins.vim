" ==================== Comandos para el manejo de Plugins ==================== "
" Check the status of your plugins
" :PlugStatus
" Install all of your plugins
" :PlugInstall
" To update your plugins
" :PlugUpdate
" After the update you can press d to see the differences or run
" :PlugDiff
" To remove plugins that are no longer defined in the plugins.vim file
" :PlugClean
" Finally if you want to upgrade vim-plug itself run the following
" :PlugUpgrade

" ========================= Configuración de Plugins ========================= "
call plug#begin('~/.config/nvim/autoload/plugged')

"   " Better Syntax Support
"   Plug 'sheerun/vim-polyglot'
    " File Explorer
    Plug 'scrooloose/NERDTree'
    " Airline
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'

call plug#end()
