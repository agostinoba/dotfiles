


"	    \                     |  _)
"	   _ \    _` |  _ \   __| __| | __ \   _ \
"	  ___ \  (   | (   |\__ \ |   | |   | (   |
"	_/    _\\__, |\___/ ____/\__|_|_|  _|\___/
"	        |___/
" Shadows

"	 ______     ______     ______     ______     ______   __     __   __     ______
"	/\  __ \   /\  ___\   /\  __ \   /\  ___\   /\__  _\ /\ \   /\ "-.\ \   /\  __ \
"	\ \  __ \  \ \ \__ \  \ \ \/\ \  \ \___  \  \/_/\ \/ \ \ \  \ \ \-.  \  \ \ \/\ \
"	 \ \_\ \_\  \ \_____\  \ \_____\  \/\_____\    \ \_\  \ \_\  \ \_\\"\_\  \ \_____\
"	  \/_/\/_/   \/_____/   \/_____/   \/_____/     \/_/   \/_/   \/_/ \/_/   \/_____/
"
" Sub-zero


"	    ___       ___       ___       ___       ___       ___       ___       ___
"	   /\  \     /\  \     /\  \     /\  \     /\  \     /\  \     /\__\     /\  \
"	  /::\  \   /::\  \   /::\  \   /::\  \    \:\  \   _\:\  \   /:| _|_   /::\  \
"	 /::\:\__\ /:/\:\__\ /:/\:\__\ /\:\:\__\   /::\__\ /\/::\__\ /::|/\__\ /:/\:\__\
"	 \/\::/  / \:\:\/__/ \:\/:/  / \:\:\/__/  /:/\/__/ \::/\/__/ \/|::/  / \:\/:/  /
"	   /:/  /   \::/  /   \::/  /   \::/  /   \/__/     \:\__\     |:/  /   \::/  /
"	   \/__/     \/__/     \/__/     \/__/               \/__/     \/__/     \/__/
"
" Smisome1


"	 _____             _   _
"	|  _  |___ ___ ___| |_|_|___ ___
"	|     | . | . |_ -|  _| |   | . |
"	|__|__|_  |___|___|_| |_|_|_|___|
"	      |___|
" Rectangles


" https://www.kammerl.de/ascii/AsciiSignature.php


" ----------------------------------- UI Configuration ----------------------------------- "

set mouse=a                 " Can use mouse on vim
syntax on
set number				    " Set number line
set numberwidth=3			" Number line width
set relativenumber			" Distance between lines and current line
set nocursorcolumn			" Highligth the current column
set nocursorline			" Highlight the current line
set nowrap				    " Wrap line
set noshowmode				" Show mode
set autoindent
set expandtab				" Expand tab to spaces so that tabs are spaces
set shiftwidth=4			" Number of spaces to use for autoindent
set softtabstop=4			" Number of spaces in tab when editing
set tabstop=4				" Number of visual spaces per tab
set ruler                   " Show cursor position
set rulerformat=%15(%c%V\ %p%%%)
set splitright				" Open new split to the right
set splitbelow              " Open new split below
set clipboard+=unnamedplus
set ignorecase              " Ignore case on search
set termguicolors
set wildmode=longest,list,full      " Show full list commands

" ======== Keyboard map ========
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" ======== Color Config ========

" gruvbox configuration
set termguicolors
set background=dark
let g:gruvbox_contrast_dark = 'soft'
colorscheme gruvbox

" ======== Autocomand ========
autocmd BufWritePre * %s/\s\+$//e       " Remove trailing whitespace on save
"
" ================================= Plugins ================================== "
source $HOME/.config/nvim/vim-plug/plugins.vim
source $HOME/.config/nvim/themes/airline.vim
